import { PrismaClient, UserModel } from "@prisma/client";
import { inject, injectable } from "inversify";
import { ILogger } from "../logger/logger.interface";
import { TYPES } from "../types";

@injectable()
export class PrismaService {
  client: PrismaClient;

  constructor(@inject(TYPES.ILogger) private logger: ILogger) {
    try {
      this.client = new PrismaClient();
      this.logger.log("[PrismaService] Успешно подключились к базе данных");
    } catch (error) {
      if (error instanceof Error) {
        this.logger.error("[PrismaService] ошибка подключения к базе данных" + error.message);
      }
    }
  }

  async connect(): Promise<void> {
    await this.client.$connect();
  }
  async disconnect(): Promise<void> {
    await this.client.$disconnect();
  }
}
