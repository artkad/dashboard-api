import { ClassConstructor, plainToClass } from "class-transformer";
import { validate } from "class-validator";
import { Request, Response, NextFunction } from "express";
import { IMiddleWare } from "./middleware.interface";

export class ValidateMiddleware implements IMiddleWare {
  constructor(private classToValidate: ClassConstructor<object>) {}
  execute({ body }: Request, res: Response, next: NextFunction): void {
    const instance = plainToClass(this.classToValidate, body);
    validate(instance).then((error) => {
      if (error.length > 0) {
        res.status(422).send(error);
      } else {
        next();
      }
    });
  }
}
