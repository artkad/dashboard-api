import { NextFunction, Request, Response, Router } from "express";
import { IMiddleWare } from "./middleware.interface";

export interface IControllerRoute {
  path: string;
  method: keyof Pick<Router, "get" | "post" | "put" | "delete" | "patch">;
  func: (req: Request, res: Response, next: NextFunction) => void;
  middlewares?: IMiddleWare[];
}

export type ExpressReturnType = Response<any, Record<string, any>>;
